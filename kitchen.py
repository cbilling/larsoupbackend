import falcon
import falcon.asgi
import pandas as pd
import copy
import time
import asyncio 
import warnings
import uuid
from coolconn import *
import subprocess
import time
import listeners
import cache
import os
from datetime import datetime, timedelta

warnings.filterwarnings('ignore')

latomePuMap = pd.read_pickle("mapping/LatomePuMap.pkl")

partitionsToMonitor = [ "ATLAS", "LArDigitalTrig", "LArAll-DT"]
class RunNumberResource():
    async def on_get(self, req, resp):

        partitionRequested = req.get_param("partition")
        resp.text = str(cache.runNumber[partitionRequested])
    
    async def updateCache(self, partition, value):
        cache.runNumber[partition] = value 

    async def on_post(self, req, resp):
        
        data = await req.get_media()
        await self.updateCache(data["partition"],data["value"])
        resp.status = falcon.HTTP_200

class LumiBlockNumberResource():
    async def on_get(self, req, resp):

        partitionRequested = req.get_param("partition")
        resp.text = str(cache.lumiBlock[partitionRequested])
    
    async def updateCache(self, partition, value):
        cache.lumiBlock[partition] = value 

    async def on_post(self, req, resp):
        
        data = await req.get_media()
        await self.updateCache(data["partition"],data["value"])
        resp.status = falcon.HTTP_200

class RCStateResource():
    async def on_get(self, req, resp):

        partitionRequested = req.get_param("partition")
        #print("herhe",cache.rcState[partitionRequested])
        resp.text = str(cache.rcState[partitionRequested])
    
    async def updateCache(self, partition, value):
        cache.rcState[partition] = value 

    async def on_post(self, req, resp):
        
        data = await req.get_media()
        await self.updateCache(data["partition"],data["value"])
        resp.status = falcon.HTTP_200

class SingleSCResource():
    
    async def on_get(self, req, resp):

        self.partitionRequested = req.get_param("partition")
        self.scRequested = req.get_param("scOnlId")
        #print(self.partitionRequested,self.scRequested)
        resp.media = await self.getCachedRate()
    
    async def getCachedRate(self):
        #print("Starting")
        listToBeReturned = []
        for step in cache.live[self.partitionRequested][::-1]:
            #print(step)
            rate = step["data"][self.scRequested]
            ts = step["ts"]
            #print(rate,ts)
            listToBeReturned.append([rate,ts])
        return listToBeReturned

class MaskedResource():

    async def on_get(self, req, resp):

        partitionRequested = req.get_param("partition")
        resp.media = cache.mask[partitionRequested]

class UpdateRatesResource():
    
    def writeToLiveCache(self,data):
        #print("trying to execute")
        #print("accesed global var")
        newData = data["data"]
        if len(cache.live[data["partition"]]) > 0:
            newData = copy.deepcopy(cache.live[data["partition"]][-1]['data'])
            newData.update(data["data"])
        cache.live[data["partition"]].append({"ts":data["ts"],"data":newData})
        cache.live[data["partition"]] = cache.live[data["partition"]][-1000::]
        #print(len(cache.live[data["partition"]]))
        return True
    
    async def on_post(self, req, resp):
        loop = falcon.get_running_loop()
        data = await req.get_media()
        #t = time.time()
        #print("here")
        if data["partition"] != "None":
            #print("next")
            done = await loop.run_in_executor(None,self.writeToLiveCache,data)
            
        #print((time.time()-t)*1000 , "ms")
        resp.text = "Thanks for data"

class getLiveRatesResource():

    async def makeListToBeReturned(self, partitionRequested, layerRequested, detectorRequested, sideRequested):
        
        listToBeReturned = []
        if len(cache.live[partitionRequested]) > 0:
            
            scs_in_region_requested = list(latomePuMap.loc[(detectorRequested,sideRequested,layerRequested),"SC_ONL_ID"])
            ratesToBeReturned = cache.live[partitionRequested][-1]["data"]
            scsToBeReturned = list(ratesToBeReturned.keys())
            good_scs = list(set(scs_in_region_requested).intersection(scsToBeReturned))
            listToBeReturned = [{"SC_ONL_ID":sc_onl_id,"rate":ratesToBeReturned[sc_onl_id]} for sc_onl_id in good_scs]
            
        return listToBeReturned
    
    async def on_get(self, req, resp):
        
        loop = falcon.get_running_loop()
        partitionRequested = req.get_param("partition")
        
        layerRequested = int(req.get_param("layer"))
        detectorRequested = int(req.get_param("det"))
        sideRequested = req.get_param("side")
        listToBeReturned = await self.makeListToBeReturned(partitionRequested, layerRequested, detectorRequested, sideRequested)
        
        resp.media = listToBeReturned

class UpdateHistResource():
    
    async def on_post(self, req, resp):
        loop = falcon.get_running_loop()
        
        try:
            data = await req.get_media()
            user_id = data["user_id"]
            runNumberRequested = data["runNumber"]
            # Now, data contains the parsed JSON data
        except Exception as e:
            resp.status = falcon.HTTP_400  # Bad Request
            textToSend += "Invalid JSON data\n"
            return
        #t = time.time()
        #print("here")
        
        done = await self.writeToHistCache(data)
            
        #print((time.time()-t)*1000 , "ms")
        resp.text = "Thanks for data"

    async def writeToHistCache(self,data):
        #print("trying to execute")
        
        #print("accesed global var")
        user_id = data["user_id"]
        runNumber = data["runNumber"]
        print(len(data["data"]))
        for timeStep in data["data"]:
            ts = timeStep["ts"]
            newData = timeStep["data"]
            #print("have stuff")
            #if len(cache.userHist[user_id][runNumber]) > 0:
                #newData = copy.deepcopy(cache.userHist[user_id][runNumber][-1])
                #newData.update(data["data"])
            
            # newData should be {"ts":1615432309,"data":{"963475":rate}}
            cache.userHist[user_id][runNumber].append({"ts":ts,"data":newData})
            # Need to sort by ts then get rid of whatever
            cache.userHist[user_id][runNumber] = cache.userHist[user_id][runNumber][-1000::]
        #print(cache.userHist[user_id][runNumber])
        print("done",data["done"])
        if data["done"]:
            cache.userWorkers[user_id] = "Not Running"
        return True
    
class HistResource():
    async def on_post(self, req, resp):

        resp.text = ""
        allowedToRetrieve = False

        # Check if user has a cookie
        user_id = req.cookies.get("user_id")
        print(user_id)
        # This user has been here before, let's see what they have so far
        if user_id is not None:
            print("trying to see user's data")
            usersData = cache.userHist.get(user_id)
            if usersData is None:
                allowedToRetrieve = True
                cache.userHist[user_id] = {}
                resp.text += "we seen this guy but he had no data\n"

            resp.text += f"Welcome back {user_id}\n"
        
        # User has not been here before, give him a cookie
        else:
            print("setting cookie")
            user_id = uuid.uuid4()
            user_id = str(user_id)
            resp.set_cookie('user_id', user_id)
            resp.text += "Cookie set with user ID\n"
            allowedToRetrieve = True
            cache.userHist[user_id] = {}
        
        # see what they sent
        try:
            print("reading data")
            
            reqData = await req.get_media()
            print(reqData)

            #reqData = json.loads(reqData)
            # Don't need partition because runNumbers are unique
            partitionRequested = reqData["partition"]
            runNumberRequested = reqData["runNumber"]
            # Now, data contains the parsed JSON data
        except Exception as e:
            print(e)
            resp.status = falcon.HTTP_400  # Bad Request
            resp.text += "Invalid JSON data\n"
            return
        
        runNumberInfo = GetRunStartStopTimes(runNumberRequested)
        startDT = runNumberInfo[0]
        endDT   = runNumberInfo[1]
        print(type(startDT),startDT)
        resp.text += f"Starting request for {runNumberRequested}\n"
        resp.text += f"Data available from {startDT} to {endDT}\n"
        print("trying to start")
        try:
            # If this user does not have a worker, they can start one
            if cache.userWorkers.get(user_id) == "Running":
                resp.text += "You already have a worker"
                return
            
            cache.userWorkers[user_id] = "Running"
            await self.startHistWorker(user_id, partitionRequested, runNumberRequested, startDT)
            print("past")
        except Exception as e:
            print(e)
        print(resp.text)
        
    async def startHistWorker(self,user_id, partitionRequested, runNumberRequested, ts):
        
        # Let's check if you don't have data
        if runNumberRequested in cache.userHist[user_id]:
            # Now check if timestamp is in the data we have
            tsList = [d["ts"] for d in cache.userHist[user_id][runNumberRequested]]
            print(tsList,ts)
            print(type(tsList[0]),ts)
            if ts in tsList:
                return "Has Data"
        
        cache.userHist[user_id][runNumberRequested] = []
        subprocess.Popen(['./startHistWorker.sh', str(user_id), str(partitionRequested), str(runNumberRequested), str(int(ts))])

    async def on_get(self, req, resp):
        loop = falcon.get_running_loop()
        try:
            tsRequested         = float(req.get_param("ts",required=True))
            runNumberRequested  = int(  req.get_param("runNumber",required=True)) # Might not be necessary since we will only have one runNumber per user
            layerRequested      = int(  req.get_param("layer",required=True))
            detectorRequested   = int(  req.get_param("det",required=True))
            sideRequested       =       req.get_param("side",required=True)
            partitionRequested  =       req.get_param("partition",required=True)
        except Exception as e:
            resp.status = falcon.HTTP_400  # Bad Request
            resp.text   = "Invalid JSON data\n"
            return

        # Check if user has cookie
            # Do nothing, they should make a post request
        
        user_id = req.cookies.get("user_id")
        if user_id is None:
            resp.text = "Make POST request to identify yourself"
            return
        
        # They have cookie but made get request before they made post request

        if user_id not in cache.userHist:
            resp.text = "Make POST request to get run " + str(runNumberRequested)
            return
        # They have cookie,
            # runNumber is not in cache, make a post request
        
        if runNumberRequested not in cache.userHist[user_id]:
            resp.text = "Make POST request to get run " + str(runNumberRequested)
            return

        # They have cookie,
            # runNumber is in cache
            # ts they want is in cache
                # map it and give it to them
            # ts they want is NOT in cache
                # tell them it needs to be requested
        print("worker status",cache.userWorkers.get(user_id))
        availTS = [d["ts"] for d in cache.userHist[user_id][runNumberRequested]]
           
        if tsRequested in availTS:
            print("available")
            try:
                listToBeReturned = await loop.run_in_executor(None,self.makeListToBeReturned, user_id, runNumberRequested, tsRequested, layerRequested, detectorRequested, sideRequested)
            except Exception as e:
                print(e)
            resp.media = listToBeReturned
            return
        
        # Recenter logic
        else:
            if cache.userWorkers.get(user_id) != "Running":
                print("starting worker from get request")
                cache.userWorkers[user_id] = "Running"
                subprocess.Popen(['./startHistWorker.sh', str(user_id), str(partitionRequested), str(runNumberRequested), str(tsRequested)])
                return

        resp.status = falcon.HTTP_404  # Bad Request
        resp.text   = f"No data for timestamp: {tsRequested}"

    def makeListToBeReturned(self, user_id, runNumberRequested, tsRequested, layerRequested, detectorRequested, sideRequested):
        
        listToBeReturned = []
        if len(cache.userHist[user_id][runNumberRequested]) > 0:
            scs_in_region_requested = list(latomePuMap.loc[(detectorRequested,sideRequested,layerRequested),"SC_ONL_ID"])
            scs_in_region_requested = list(map(str,scs_in_region_requested))
            t = time.time()
            for rates in cache.userHist[user_id][runNumberRequested]:
                #print(rates["ts"],tsRequested)
                if rates["ts"] == tsRequested:
                    ratesToBeReturned = rates["data"]
                    break
            
            scsToBeReturned = list(ratesToBeReturned.keys())
            good_scs = list(set(scs_in_region_requested).intersection(scsToBeReturned))
            listToBeReturned = [{"SC_ONL_ID":sc_onl_id,"rate":ratesToBeReturned[sc_onl_id]} for sc_onl_id in good_scs]
            
        return listToBeReturned

class ListenerResource():
    async def on_get(self, req, resp):

        #print(asyncio.all_tasks())
        tasks = [task.get_name() for task in asyncio.all_tasks() if not task.done()]
        #resp.text = "\n".join(tasks)
        statuses = {}
        for t in tasks:
            if "Task" not in t:
                if len(t.split("-")) > 2:
                    partition = t.split("-")[1] + "-" + t.split("-")[2]
                else:
                    partition = t.split("-")[1]
                if partition not in statuses:
                    statuses[partition] = []
                l = {}
                #print(t,cache.listenerStatuses.get(t))
                l['listenerName'] = t
                l['status'] = cache.listenerStatuses.get(t,"Not Running")
                statuses[partition].append(l)
        #print(statuses)
        default = copy.deepcopy(cache.defaultListeners)
        default.update(statuses)
        #print(default)
        resp.media = default

    async def on_post(self, req, resp):

        data = await req.get_media()
        #print(data)
        action = data['action']
        listenerName = data['listenerName']
        partition = data['partition']
        taskName = listenerName+"-"+partition
        
        if action == "Cancel":
            if listenerName.lower() == "all":
                for task in asyncio.all_tasks():
                    if task.get_name().split("-")[1] == partition:
                        wasCancelled = task.cancel()
                resp.status = falcon.HTTP_200
                resp.text = f"Cancelled all tasks on {partition} partition"
                return
            for task in asyncio.all_tasks():
                if not task.done():
                    
                    if task.get_name() == taskName:
                        wasCancelled = task.cancel()
                        resp.status = falcon.HTTP_200
                        resp.text = f"Cancelled {taskName}"
                        return
                    

        if action == "Start":
            # Check if task is already running
            for task in asyncio.all_tasks():
                if not task.done():
                    if task.get_name() == taskName:
                        resp.status = falcon.HTTP_202
                        resp.text = "Task already running"
                        return

            resp.status = falcon.HTTP_405
            resp.text = f"Listener {taskName} not available to be started"        
            # If we are here, then we can make the task
            if listenerName == "rcListener":
                asyncio.create_task(listeners.rcStateSSE(partition),name="rcListener-"+partition)
            elif listenerName == "runNumListener":
                asyncio.create_task(listeners.runNumberSSE(partition),name="runNumListener-"+partition)
            elif listenerName == "lbListener":
                asyncio.create_task(listeners.lumiBlockSSE(partition),name="lbListener-"+partition)
            elif listenerName == "ratesListener":
                asyncio.create_task(listeners.ratesSSE(partition),name="ratesListener-"+partition)
            elif listenerName == "masksListener":
                asyncio.create_task(listeners.ratesSSE(partition),name="masksListener-"+partition)    
            elif listenerName.lower() == "all":
                # Check if they are running already
                running_tasks = [task.get_name() for task in asyncio.all_tasks() if not task.done()]
                if "rcListener-"+partition not in running_tasks:
                    asyncio.create_task(listeners.rcStateSSE(partition),name="rcListener-"+partition)
                if "runNumListener-"+partition not in running_tasks:
                    asyncio.create_task(listeners.runNumberSSE(partition),name="runNumListener-"+partition)
                if "lbListener-"+partition not in running_tasks:
                    asyncio.create_task(listeners.lumiBlockSSE(partition),name="lbListener-"+partition)
                if "ratesListener-"+partition not in running_tasks:
                    asyncio.create_task(listeners.ratesSSE(partition),name="ratesListener-"+partition)
                if "masksListener-"+partition not in running_tasks:
                    asyncio.create_task(listeners.masksSSE(partition),name="masksListener-"+partition)
            else:
                return
            
            resp.status = falcon.HTTP_200
            resp.text = f"Started {taskName}"

getLiveRates    = getLiveRatesResource()
runNumber       = RunNumberResource()
lumiBlockNumber = LumiBlockNumberResource()
rcState         = RCStateResource()
singleSC        = SingleSCResource()
updateLiveRates = UpdateRatesResource()
updateHistRates = UpdateHistResource()
hist            = HistResource()
listener        = ListenerResource()
masks           = MaskedResource()

app = falcon.asgi.App(middleware=falcon.CORSMiddleware(allow_origins='*'))

app.add_route('/rates', getLiveRates)
app.add_route('/runNumber', runNumber)
app.add_route('/lumiBlockNumber',lumiBlockNumber)
app.add_route('/rcState',rcState)
app.add_route('/singleScRates',singleSC)
app.add_route('/updateRates', updateLiveRates)
app.add_route('/updateHistRates',updateHistRates)
app.add_route('/hist', hist)
app.add_route('/checkListeners', listener)
app.add_route('/masks', masks)

app.add_static_route('/static/','/home/cbilling/larsoupbackend/static/')
async def cookie_refresher():
    while True:
            
        os.system("""
        kdestroy
        kinit -kt /home/cbilling/cbilling.keytab cbilling
        klist
        source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-01-00/installed/setup.sh
        auth-get-sso-cookie -u https://atlasop.cern.ch -o cookiefile.txt
        """)
        await asyncio.sleep(60*60)

asyncio.create_task(cookie_refresher())
for partition in partitionsToMonitor:
    
    asyncio.create_task(listeners.rcStateSSE(partition),name="rcListener-"+partition)
    asyncio.create_task(listeners.runNumberSSE(partition),name="runNumListener-"+partition)
    asyncio.create_task(listeners.lumiBlockSSE(partition),name="lbListener-"+partition)
    asyncio.create_task(listeners.ratesSSE(partition),name="ratesListener-"+partition)
    asyncio.create_task(listeners.masksSSE(partition),name="masksListener-"+partition)
    
print("started state")
