#!/bin/sh

export PYTHONUNBUFFERED=1

#if [ $# -ne 1 ]; then
#    echo "Usage: $0 <user_id> <partition> <runNumber> <ts>"
#    exit 1
#fi
start_time=$(date +%s)
# Assign the argument to a variable
user_id="$1"
partition="$2"
runNumber="$3"
ts="$4"

echo starting 
kdestroy
klist
kinit -kt /home/cbilling/cbilling.keytab cbilling
klist
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-01-00/installed/setup.sh

# Print the elapsed time
python histWorker.py -u $user_id -p $partition -rn $runNumber -t $ts
end_time=$(date +%s)
elapsed_time=$((end_time - start_time))
echo "Elapsed time: ${elapsed_time} seconds"
