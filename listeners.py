# will come back to this
import kitchen
import cache
import argparse
import asyncio
import re
import httpx
import certifi
import time
from aiosseclient import aiosseclient
import json
import pandas
import xmltodict
import pprint
import datetime
import copy


def parseCookieFile(cookiefile):
    """Parse a cookies.txt file and return a dictionary of key value pairs
    compatible with requests."""

    cookies = {}
    with open (cookiefile, 'r') as fp:
        for line in fp:
            
            if not re.match(r'^\#', line):
                lineFields = line.strip().split('\t')
                
                try:
                    cookies[lineFields[5]] = lineFields[6]
                except:
                    pass
    return cookies

cookies = parseCookieFile('cookiefile.txt')


async def rcStateSSE(partition):
    try:
        cache.listenerStatuses['rcListener-'+partition] = "Initializing"
        cookies = parseCookieFile('cookiefile.txt')
        r = httpx.get(f'https://atlasop.cern.ch/info/current/{partition}/is/RunCtrl/RunCtrl.RootController',cookies=cookies,verify=False)
        response = xmltodict.parse(r.text)
        if "objects" in response:
            attributes = response["objects"]["obj"]["attr"]
            for a in attributes:
                if a["@name"] == "state":
                    value = a["v"]
                    cache.rcState[partition] = value 
        cache.listenerStatuses['rcListener-'+partition] = "Initialized"
    except:
        cache.listenerStatuses['rcListener-'+partition] = "Could not initialize"
        print("Partition not available")  

    print("Connecting")
    while True:
        try:
            cache.listenerStatuses['rcListener-'+partition] = "Starting listener connection"
            cookies = parseCookieFile('cookiefile.txt')
            async for event in aiosseclient(f'https://atlasop.cern.ch/info/current/{partition}/is/RunCtrl/RunCtrl.RootController?listen=update&format=json',cookies=cookies):
                if len(event.data) > 0:
                    d = json.loads(event.data)
                    for i in d:
                        if isinstance(i,dict):
                            if i["name"] == "state":
                                value = i["value"]
                                cache.rcState[partition] = value 
                                cache.listenerStatuses['rcListener-'+partition] = "Listener connection alive"
        
        except Exception as e:
            print(e)
        cache.listenerStatuses['rcListener-'+partition] = "Connection to IS died"
        await asyncio.sleep(15)


async def runNumberSSE(partition):
    
    try:
        cache.listenerStatuses['runNumListener-'+partition] = "Initializing"
        cookies = parseCookieFile('cookiefile.txt')
        r = httpx.get(f'https://atlasop.cern.ch/info/current/{partition}/is/RunParams/RunParams.LumiBlock',cookies=cookies,verify=False)
        response = xmltodict.parse(r.text)
        if "objects" in response:
            attributes = response["objects"]["obj"]["attr"]
            for a in attributes:
                if a["@name"] == "RunNumber":
                    value = a["v"]
                    cache.runNumber[partition] = value
        cache.listenerStatuses['runNumListener-'+partition] = "Initialized"
                 
    except:
        cache.listenerStatuses['runNumListener-'+partition] = "Could not initialize"
        print("Partition not available")  

    while True:
        try:
            cache.listenerStatuses['runNumListener-'+partition] = "Starting listener connection"
            cookies = parseCookieFile('cookiefile.txt')
            async for event in aiosseclient(f'https://atlasop.cern.ch/info/current/{partition}/is/RunParams/RunParams.LumiBlock?listen=update&format=json',cookies=cookies):
                if len(event.data) > 0:
                    d = json.loads(event.data)
                    for i in d:
                        if isinstance(i,dict):
                            if i["name"] == "RunNumber":
                                value = i["value"]
                                cache.runNumber[partition] = value
                                cache.listenerStatuses['runNumListener-'+partition] = "Listener connection alive"
        
        except Exception as e:
            print(e)
        cache.listenerStatuses['runNumListener-'+partition] = "Connection to IS died"
        await asyncio.sleep(15)


async def lumiBlockSSE(partition):

    try:
        cache.listenerStatuses['lbListener-'+partition] = "Initializing"
        cookies = parseCookieFile('cookiefile.txt')
        r = httpx.get(f'https://atlasop.cern.ch/info/current/{partition}/is/RunParams/RunParams.LumiBlock',cookies=cookies,verify=False)
        response = xmltodict.parse(r.text)
        if "objects" in response:
            attributes = response["objects"]["obj"]["attr"]
            for a in attributes:
                if a["@name"] == "LumiBlockNumber":
                    value = a["v"]
                    cache.lumiBlock[partition] = value
        cache.listenerStatuses['lbListener-'+partition] = "Initialized"
        
    except:
        cache.listenerStatuses['lbListener-'+partition] = "Could not initialize"
        print("Partition not available")  

    while True:
        try:
            cache.listenerStatuses['lbListener-'+partition] = "Starting listener connection"
            cookies = parseCookieFile('cookiefile.txt')
            async for event in aiosseclient(f'https://atlasop.cern.ch/info/current/{partition}/is/RunParams/RunParams.LumiBlock?listen=update&format=json',cookies=cookies):
                if len(event.data) > 0:
                    d = json.loads(event.data)
                    for i in d:
                        if isinstance(i,dict):
                            if i["name"] == "LumiBlockNumber":
                                value = i["value"]
                                cache.lumiBlock[partition] = value
                                cache.listenerStatuses['lbListener-'+partition] = "Listener connection alive"
            
        except Exception as e:
            print(e)
        cache.listenerStatuses['lbListener-'+partition] = "Connection to IS died"
        await asyncio.sleep(15)
    

async def ratesSSE(partition):
    while True:
        try:
            cache.listenerStatuses['ratesListener-'+partition] = "Starting listener connection"  
            cookies = parseCookieFile('cookiefile.txt')
            async for event in aiosseclient(f'https://atlasop.cern.ch/info/current/{partition}/is/LArMonParams/LArMonParams.LAr.LDPB..*.PulseRates?listen=update&format=json&multiple=1',cookies=cookies):
                if len(event.data) > 0:    
                    mappedRates = {}
                    #d = json.loads("""[ "LArMonParams.LAr.LDPB.LArC_EMBC_EMECC_1.PulseRates", "RatesLArC", "28/10/23 16:38:34.194679", { "name": "LArCName", "descr": "LArCname", "type": "string", "range": "",  "value": "LArC_EMBC_EMECC_1"},{ "name": "IntegrationTime", "descr": "integration time used to calculate rates (in s)", "type": "u32", "range": "",  "value": 1},{ "name": "MonitorMode", "descr": "Mode of the pulse rate monitor: 0 = off, 1 = read sum of all SCs, 2 = sequencially read each of the 6 SCs", "type": "u32", "range": "",  "value": 2},{ "name": "RatesLatomes", "descr": "Latomes in the LArC", "type": "RatesLatome", "range": "",  "value": [[ "", "RatesLatome", "28/10/23 16:38:34.198942", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_1"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [110,46,38,46,46,136,136,142,137,185,110,33,46,38,52,138,149,148,147,230,103,44,41,46,54,102,113,96,121,141,119,48,50,31,52,113,128,99,120,149,113,48,47,51,52,146,148,140,146,218,240,121,39,48,50,48,143,135,158,145,240,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198945", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_2"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [114,51,58,45,46,78,143,140,140,213,108,45,49,46,47,142,134,141,183,184,120,57,41,50,41,101,117,131,84,152,121,72,48,52,47,117,135,99,145,140,109,55,47,46,47,131,106,137,153,238,240,110,48,48,42,44,154,135,147,110,224,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198948", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_3"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [112,48,47,46,48,120,175,147,155,193,99,45,51,47,43,133,134,131,139,149,115,53,56,40,46,109,113,98,121,124,122,48,45,71,38,118,123,130,109,152,106,54,46,47,47,143,141,138,132,232,240,101,34,36,45,45,112,173,137,131,246,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198951", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_4"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [113,47,43,47,41,126,129,135,102,177,110,30,49,44,43,137,137,128,118,189,113,36,48,46,46,116,114,124,126,150,128,48,52,44,43,122,119,109,136,162,107,47,44,37,46,126,136,119,129,245,240,122,50,42,45,47,150,109,150,137,304,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}]]}]""")
                    d = json.loads(event.data)
                    for i in d:
                        if isinstance(i,dict):
                            if i["name"] == "RatesLatomes":
                                ratesInLArC = i["value"]
                    ts = datetime.datetime.strptime(ratesInLArC[0][2],"%d/%m/%y %H:%M:%S.%f")
                    for latome in ratesInLArC:
                        for i in latome:
                            if isinstance(i,dict):
                                if i['name'] == "LATOMEName":
                                    latomeName = i["value"]
                                elif i["name"] == "superCell":
                                    # assuming superCell is 1-indexed
                                    superCell = int(i["value"]) + 1
                                elif i["name"] == "streamRates":
                                    ratesFromPUs = i["value"] 
                        if latomeName != "DISABLED":
                            for pu, rate in enumerate(ratesFromPUs):
                                try:
                                    sc_onl_id = cache.latomePuMap.loc[(latomeName,superCell, pu + 1),"SC_ONL_ID"]
                                    mappedRates[int(sc_onl_id)] = rate
                                    
                                except Exception as e:
                                    #print(e)
                                    pass
                    
                    newData = mappedRates
                    if len(cache.live[partition]) > 0:
                        newData = copy.deepcopy(cache.live[partition][-1]['data'])
                        newData.update(mappedRates)
                    cache.live[partition].append({"ts":int(datetime.datetime.timestamp(ts)),"data":newData})
                    cache.live[partition] = cache.live[partition][-1000::]
                    cache.listenerStatuses['ratesListener-'+partition] = "Listener connection alive"
            
        except Exception as e:
            print(e)
        cache.listenerStatuses['ratesListener-'+partition] = "Connection to IS died"
        await asyncio.sleep(15)

async def masksSSE(partition):
    while True:
        try:
            cache.listenerStatuses['masksListener-'+partition] = "Starting listener connection"  
            cookies = parseCookieFile('cookiefile.txt')
            async for event in aiosseclient(f'https://atlasop.cern.ch/info/current/{partition}/is/LArMonParams/LArMonParams.LAr.LDPB..*.PulseRates?listen=update&format=json&multiple=1',cookies=cookies):
                if len(event.data) > 0:    
                    mappedRates = {}
                    #d = json.loads("""[ "LArMonParams.LAr.LDPB.LArC_EMBC_EMECC_1.PulseRates", "RatesLArC", "28/10/23 16:38:34.194679", { "name": "LArCName", "descr": "LArCname", "type": "string", "range": "",  "value": "LArC_EMBC_EMECC_1"},{ "name": "IntegrationTime", "descr": "integration time used to calculate rates (in s)", "type": "u32", "range": "",  "value": 1},{ "name": "MonitorMode", "descr": "Mode of the pulse rate monitor: 0 = off, 1 = read sum of all SCs, 2 = sequencially read each of the 6 SCs", "type": "u32", "range": "",  "value": 2},{ "name": "RatesLatomes", "descr": "Latomes in the LArC", "type": "RatesLatome", "range": "",  "value": [[ "", "RatesLatome", "28/10/23 16:38:34.198942", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_1"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [110,46,38,46,46,136,136,142,137,185,110,33,46,38,52,138,149,148,147,230,103,44,41,46,54,102,113,96,121,141,119,48,50,31,52,113,128,99,120,149,113,48,47,51,52,146,148,140,146,218,240,121,39,48,50,48,143,135,158,145,240,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,10000,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198945", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_2"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [114,51,58,45,46,78,143,140,140,213,108,45,49,46,47,142,134,141,183,184,120,57,41,50,41,101,117,131,84,152,121,72,48,52,47,117,135,99,145,140,109,55,47,46,47,131,106,137,153,238,240,110,48,48,42,44,154,135,147,110,224,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198948", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_3"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [112,48,47,46,48,120,175,147,155,193,99,45,51,47,43,133,134,131,139,149,115,53,56,40,46,109,113,98,121,124,122,48,45,71,38,118,123,130,109,152,106,54,46,47,47,143,141,138,132,232,240,101,34,36,45,45,112,173,137,131,246,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}],[ "", "RatesLatome", "28/10/23 16:38:34.198951", { "name": "LATOMEName", "descr": "LATOME name", "type": "string", "range": "",  "value": "LATOME_EMBC_EMECC_4"},{ "name": "superCell", "descr": "Selected super cell (0 to 5) for the the rate scan. 6 is for the total rate of the sum of the 6 SCs", "type": "u32", "range": "",  "value": 3},{ "name": "EnergyThresholds", "descr": "Energy thresholds used in each of the 62 streams for the scanned SC", "type": "u32", "range": "",  "value": [113,47,43,47,41,126,129,135,102,177,110,30,49,44,43,137,137,128,118,189,113,36,48,46,46,116,114,124,126,150,128,48,52,44,43,122,119,109,136,162,107,47,44,37,46,126,136,119,129,245,240,122,50,42,45,47,150,109,150,137,304,240]},{ "name": "streamRates", "descr": "Rates in each of the 62 streams", "type": "u32", "range": "",  "value": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}]]}]""")
                    d = json.loads(event.data)
                    for i in d:
                        if isinstance(i,dict):
                            if i["name"] == "RatesLatomes":
                                ratesInLArC = i["value"]
                    ts = datetime.datetime.strptime(ratesInLArC[0][2],"%d/%m/%y %H:%M:%S.%f")
                    for latome in ratesInLArC:
                        for i in latome:
                            if isinstance(i,dict):
                                if i['name'] == "LATOMEName":
                                    latomeName = i["value"]
                                elif i["name"] == "superCell":
                                    # assuming superCell is 1-indexed
                                    superCell = int(i["value"]) + 1
                                elif i["name"] == "streamRates":
                                    ratesFromPUs = i["value"] 
                        if latomeName != "DISABLED":
                            for pu, rate in enumerate(ratesFromPUs):
                                try:
                                    sc_onl_id = cache.latomePuMap.loc[(latomeName,superCell, pu + 1),"SC_ONL_ID"]
                                    mappedRates[int(sc_onl_id)] = rate
                                    
                                except Exception as e:
                                    #print(e)
                                    pass
                    
                    newData = mappedRates
                    if len(cache.live[partition]) > 0:
                        newData = copy.deepcopy(cache.live[partition][-1]['data'])
                        newData.update(mappedRates)
                    cache.live[partition].append({"ts":int(datetime.datetime.timestamp(ts)),"data":newData})
                    cache.live[partition] = cache.live[partition][-1000::]
                    cache.listenerStatuses['masksListener-'+partition] = "Listener connection alive"
            
        except Exception as e:
            print(e)
        cache.listenerStatuses['masksListener-'+partition] = "Connection to IS died"
        await asyncio.sleep(15)
