import pandas
live       = {"initial": [],       "ATLAS": [],        "LArDigitalTrig": [],       "LArAll-DT": []}
runNumber  = {"initial": 0,        "ATLAS": 0,         "LArDigitalTrig": 0,        "LArAll-DT": 0}
rcState    = {"initial": "None",   "ATLAS": "None",    "LArDigitalTrig": "None",   "LArAll-DT": "None"}
lumiBlock  = {"initial": 0,        "ATLAS": 0,         "LArDigitalTrig": 0,        "LArAll-DT": 0}
mask       = {"initial": [],       "ATLAS": [],        "LArDigitalTrig": [],       "LArAll-DT": []}

userHist = {}
defaultListeners = {
    'LArDigitalTrig': [
        {'listenerName': 'rcListener-LArDigitalTrig', 'status': 'Not Running'}, 
        {'listenerName': 'runNumListener-LArDigitalTrig', 'status': 'Not Running'}, 
        {'listenerName': 'lbListener-LArDigitalTrig', 'status': 'Not Running'}, 
        {'listenerName': 'ratesListener-LArDigitalTrig', 'status': 'Not Running'},
        {'listenerName': 'masksListener-LArDigitalTrig', 'status': 'Not Running'}
    ], 
    'initial': [
        {'listenerName': 'lbListener-initial', 'status': 'Not Running'}, 
        {'listenerName': 'ratesListener-initial', 'status': 'Not Running'}, 
        {'listenerName': 'runNumListener-initial', 'status': 'Not Running'}, 
        {'listenerName': 'rcListener-initial', 'status': 'Not Running'},
        {'listenerName': 'masksListener-initial', 'status': 'Not Running'}
    ], 
    'LArAll-DT': [
        {'listenerName': 'rcListener-LArAll-DT', 'status': 'Not Running'}, 
        {'listenerName': 'runNumListener-LArAll-DT', 'status': 'Not Running'}, 
        {'listenerName': 'lbListener-LArAll-DT', 'status': 'Not Running'}, 
        {'listenerName': 'ratesListener-LArAll-DT', 'status': 'Not Running'},
        {'listenerName': 'masksListener-LArAll-DT', 'status': 'Not Running'}
    ], 
    'ATLAS': [
        {'listenerName': 'rcListener-ATLAS', 'status': 'Not Running'}, 
        {'listenerName': 'runNumListener-ATLAS', 'status': 'Not Running'}, 
        {'listenerName': 'lbListener-ATLAS', 'status': 'Not Running'}, 
        {'listenerName': 'ratesListener-ATLAS', 'status': 'Not Running'},
        {'listenerName': 'masksListener-ATLAS', 'status': 'Not Running'}
    ]}
listenerStatuses = {}

latomePuMap = pandas.read_csv("mapping/test.csv")
latomePuMap.set_index(["LATOME_NAME","SCnum","PU"],inplace=True)

userWorkers = {}