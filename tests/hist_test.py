# Basic test:
#   User with and without a cookie:
#       Requests runNumber
#       Sends get requests for rates, walking through time
#       if it gets 1 minute to edge of availibity, recenter
#       let it run for 4 minutes
# Jump around test:
#   User with and without a cookie:
#       Requests runNumber
#       start jumping around
#           go within 1 minute of availibility, recenter
#           go way to the end of the run, recenter
#       If user jumps around too much:
#           rate limiting 
#           cancel 

# If at any point it needs to reset, there needs to be logic to prevent duplicate queries
import httpx
import time
class ClientTester():
    def __init__(self, startWithCookie = False,cookies = None):
        if startWithCookie:
            assert isinstance(cookies, httpx.Cookies())
            self.cookies = cookies
            
        
    def postRunNumber(self,runNumber):
        print(f"Sending runNumber: {runNumber}")
        self.runNumber = runNumber
        r = httpx.post("https://atlas-larmon-dev.cern.ch/dev/LArSoup/hist",json={"runNumber":runNumber,"partition":"ATLAS"},verify=False)
        print(f"Response: {r.text}")
        self.cookies = r.cookies
        print(self.cookies.keys())
        print(f"Cookies: {self.cookies}")
        assert self.cookies.get("user_id")
        return r.text
    
    def getHistRates(self,ts = 0,cookie = None):
        print(f"Getting rates")
        for i in range(10):

            for l in range(1):
                for d in range(1):
                    for s in ["A"]:
                        params = {
                            "ts":ts+i,
                            "runNumber":463124,
                            "layer":l,
                            "det":d,
                            "side":s,
                            "partition":"ATLAS"
                        }
                        if isinstance(cookie,type(None)):
                            c = self.cookies
                        else:
                            c = cookie
                        r = httpx.get("https://atlas-larmon-dev.cern.ch/dev/LArSoup/hist",cookies=c,params=params,verify=False)
                        try:
                            print(f"Response: {len(r.json())}")
                        except:
                            print("no data")
            time.sleep(3)

    def basicTest(self):

        responseText = self.postRunNumber(463124)
        
        runStartTime = responseText.split("\n")[2].split(" ")[3]
        print(responseText.split("\n")[2].split(" ")[3])
        time.sleep(60)
        self.getHistRates(ts = runStartTime)#cookie={"user_id":"c6ca1db8-5546-4642-abd8-5f52c62478d2"})
bt = ClientTester()
bt.basicTest()