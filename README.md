# LarSoupbackend

This repository serves as the backend to the application run here: https://atlas-larmon.cern.ch/LArWebApp/

It is an asynchronous python web server designed with Falcon and run using uvicorn. The listeners are written 
in listeners.py but started using asyncio tasks in kitchen.py. These listeners directly modify variables in the 
cache.py file and the variables are read by methods in the Falcon server. The mapping to be able to map the IS 
rates to SuperCells is stored in mapping/. 

The listeners need to send cookies to be able to authenticate with the Web IS server. The command to refresh this
token is run once every 5 hours and each listener parses this cookie file everytime it goes to connect or
reconnect with the Web IS server. These listeners can also be monitored, started, and canceled by going to 
the url of the backend, for the dev case it is https://atlas-larmon-dev.cern.ch/dev/LArSoup/static/index.html.
From here, the 4 default partitions, initial, ATLAS, LArAll-DT, and LArDigitalTrig, and their 4 correpsonding 
listeners, rates, rcstate, lumiblock, and runnumber are always displayed. A form below these statuses can be 
submitted to start or cancel these listeners. To start or cancel all listeners of a partition, the listener
field should be filled with the word "all". This functionality can also be used to start listening to 
a partition that is not default.

To run the application:
source setup.sh
python3 -m uvicorn kitchen:app --port 17679 --workers 1 --log-level error
