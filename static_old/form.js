async function fillTheTitle() {
  const post = await fetch("https://atlas-larmon-dev.cern.ch/dev/LArSoup/checkListeners").then((res) => res.json());
  
  const oldDiv = document.getElementById("result");
  for (var key in post) {
      if (post.hasOwnProperty(key)) {
          const boxes = document.createElement("div");
          boxes.classList.add("cls-context-menu");
          const node = document.createElement("H");                // Create a <p> node
          const textNode = document.createTextNode(key);      // Create a text node
          node.appendChild(textNode);                              // Append the text to your <p> node
          boxes.appendChild(node);  
          post[key].forEach((obj) => {
              
              const node = document.createElement("div");                // Create a <p> node
              node.classList.add("cls-context-menu");
              const textNode = document.createTextNode(obj.listenerName+":  "+obj.status);      // Create a text node
              node.appendChild(textNode);                              // Append the text to your <p> node
              node.setAttribute("id", obj.listenerName);                         // Create an id
              if (obj.status == "Connection to IS died") {
                node.style.backgroundColor = "red"
              } else if (obj.status == "Not Running") {
                node.style.backgroundColor = "grey"
              } else if (obj.status == "Starting listener connection") {
                node.style.backgroundColor = "yellow"
              } else if (obj.status == "Listener connection alive") {
                node.style.backgroundColor = "green"
              } else {
                node.style.backgroundColor = "light grey"
              }
                
              boxes.appendChild(node);  
          });
        var el = document.getElementById(key)
        boxes.setAttribute('id',key)
        if (el == null){
          document.body.insertBefore(boxes, oldDiv);
        } else {
          el.replaceWith(boxes)
        }
    } 
  }
  
  // boxes.setAttribute('id','result')
  // app.replaceWith(boxes)
  
  // document.getElementById("result").innerText = JSON.stringify(post);

  }
  
    
function submitForm(e) {
    // e.preventDefault();
    
    // var myform =    document.getElementById("myform");
    // console.log(myform)
    let action = document.getElementById("myform_action");
    let listenerName = document.getElementById("myform_listenerName");
    let partition = document.getElementById("myform_partition");

    console.log(action.value)
    console.log(listenerName.value)
    console.log(partition.value)
    var formData = new FormData();
    formData.append("action", action.value);
    formData.append("listenerName", listenerName.value);
    formData.append("partition", partition.value);
    // Display the key/value pairs
    for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }
    var details = {"action": action.value,"listenerName": listenerName.value,"partition": partition.value}
    var formBody = [];
    for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    // console.log(formData)
    fetch("https://atlas-larmon-dev.cern.ch/dev/LArSoup/checkListeners", {
        method: "POST",
        body: formBody,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        mode: "cors"
    })
        .then(response => {
        if (!response.ok) {
        throw new Error('network returns error');
        }
        return response.text();
    })
        .then((resp) => {
        let respdiv = document.getElementById("submitResult");
        respdiv.innerText = resp;
        
        console.log("resp from server ", resp);
        })
        .catch((error) => {
        // Handle error
        let respdiv = document.getElementById("submitResult");
        respdiv.innerText = error;
        
        //console.log("error ", error);
        });
    }

var myform = document.getElementById("myform");

myform.addEventListener("submit", submitForm);
myform.addEventListener("submit", fillTheTitle);
