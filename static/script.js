document.addEventListener('DOMContentLoaded', () => {
    const jobContainer = document.getElementById('job-container');

    // Function to update job statuses
    async function updateJobStatuses() {
        // Simulated job data
        const partitionsWithJobs = await fetch("https://atlas-larmon-dev.cern.ch/dev/LArSoup/checkListeners").then((res) => res.json());
        for (var partition in partitionsWithJobs) {
            if (partitionsWithJobs.hasOwnProperty(partition)) {
                
                const boxes = document.createElement("div");
                const node = document.createElement("H");                // Create a <p> node
                const textNode = document.createTextNode(partition);      // Create a text node
                node.appendChild(textNode);                              // Append the text to your <p> node
                boxes.appendChild(node);  
                boxes.setAttribute('id',partition)
                
                // Add contextmenu event listener for right-click
                boxes.addEventListener('contextmenu', (event) => {
                    
                    // Create context menu
                    const contextMenu = document.createElement('div');
                    contextMenu.classList.add('context-menu');
                    const startOption = document.createElement('div');
                    startOption.innerText = 'Start';
                    startOption.addEventListener('click', () => {
                        sendPostRequest("all-"+boxes.id, 'Start');
                        closeContextMenu();
                    });
                    const stopOption = document.createElement('div');
                    stopOption.innerText = 'Stop';
                    stopOption.addEventListener('click', () => {
                        sendPostRequest("all-"+boxes.id, 'Cancel');
                        closeContextMenu();
                    });

                    // Append options to context menu
                    contextMenu.appendChild(startOption);
                    contextMenu.appendChild(stopOption);

                    // Position context menu
                    contextMenu.style.top = `${event.clientY}px`;
                    contextMenu.style.left = `${event.clientX}px`;

                    // Add context menu to the document
                    document.body.appendChild(contextMenu);

                    // Close context menu when clicking outside
                    document.addEventListener('click', closeContextMenu);
                });
                // Create job boxes
                partitionsWithJobs[partition].forEach(job => {
                    const box = document.createElement('div');
                    box.classList.add('job-box', `job-${job.status.replace(/ /g, "_")}`);
                    box.innerText = `${job.listenerName.split("-")[0]}`;

                    // Add contextmenu event listener for right-click
                    box.addEventListener('contextmenu', (event) => {
                        
                        // Create context menu
                        const contextMenu = document.createElement('div');
                        contextMenu.classList.add('context-menu');
                        const startOption = document.createElement('div');
                        startOption.innerText = 'Start';
                        startOption.addEventListener('click', () => {
                            sendPostRequest(job.listenerName, 'Start');
                            closeContextMenu();
                        });
                        const stopOption = document.createElement('div');
                        stopOption.innerText = 'Stop';
                        stopOption.addEventListener('click', () => {
                            sendPostRequest(job.listenerName, 'Cancel');
                            closeContextMenu();
                        });

                        // Append options to context menu
                        contextMenu.appendChild(startOption);
                        contextMenu.appendChild(stopOption);

                        // Position context menu
                        contextMenu.style.top = `${event.clientY}px`;
                        contextMenu.style.left = `${event.clientX}px`;

                        // Add context menu to the document
                        document.body.appendChild(contextMenu);

                        // Close context menu when clicking outside
                        document.addEventListener('click', closeContextMenu);
                    });

                    boxes.appendChild(box);
                });
                
                var el = document.getElementById(partition)
                boxes.setAttribute('id',partition)
                if (el == null){
                document.body.insertBefore(boxes, jobContainer);
                } else {
                el.replaceWith(boxes)
                }
            }
            
        }
        
    }

    // Function to send POST request
    function sendPostRequest(listenerNameAndPartition, action) {
        const partition = listenerNameAndPartition.split("-")[1]
        const listenerName = listenerNameAndPartition.split("-")[0]
        var details = {"action": action,"listenerName": listenerName,"partition": partition}
        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
    
        fetch("https://atlas-larmon-dev.cern.ch/dev/LArSoup/checkListeners", {
            method: 'POST',
            body: formBody,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            mode: "cors"
        })
        .then(response => {
            if (!response.ok) {
                throw new Error(`Failed to ${action} job ${listenerName}`);
            }
            return response.text();
        })
        .then(data => {
            console.log(`Job ${listenerName} ${action} successful:`, data);
            // Update job status on the frontend if needed
        })
        .catch(error => {
            console.error(`Error while trying to ${action} job ${listenerName}:`, error);
        });
        location.reload();
    }

    // Function to close the context menu
    function closeContextMenu() {
        const contextMenu = document.querySelector('.context-menu');
        if (contextMenu) {
            contextMenu.remove();
        }
        document.removeEventListener('click', closeContextMenu);
    }

    // Initial update of job statuses
    updateJobStatuses();

    // Periodically update job statuses every second
    setInterval(updateJobStatuses, 1000);
});
