def us_since_epoch(time_str, add_time=0):
    return int(
        datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S").timestamp() * 1e6
    ) + int(add_time * 1e6)

with open("latome_to_pbeast_key.json") as f:
    latomeToPbeast = json.load(f)

latomePuMap = pd.read_csv('LatomePUMap.txt',delim_whitespace=True,header=None)
latomePuMap.columns = ["LATOME_NAME", "PU", "SCnum", "SC_ONL_ID", "SAM", "SCNAME",  "DET", "AC","TT_COOL_ID",  "SCETA", "SCPHI", "LATOME_FIBRE"]
latomePuMap.drop(["SCNAME", "TT_COOL_ID",  "SCETA", "SCPHI", "LATOME_FIBRE"],axis=1,inplace=True)
latomePuMap['SAMtrue'] = 5
new_rows = []
for index, row in latomePuMap.iterrows():
    #print(index)
    layers = format(row["SAM"] ,'#06b')
    binLayers = layers.split("b")[-1][::-1]

    for sam, b in enumerate(binLayers):
        b = int(b)
        
        if b:
            new = list(row.values)
            new[-1] = sam
            new_rows.append(new)
df_new = pd.DataFrame(new_rows, columns=latomePuMap.columns)
df_new.set_index(["DET","AC","SAMtrue"],inplace=True)
df_new.to_pickle("LatomePuMap.pkl")
print(df_new.head(20))
latomePbKeyList = [latomeToPbeast[i] for i in list(latomePuMap['LATOME_NAME'])]
#                                           0                           1                                           2                                   3                           4                              5                             6
fastIterPuMap = list(zip(latomePbKeyList,list(latomePuMap['PU'].astype("int")),list(latomePuMap['SCnum']),list(latomePuMap['SC_ONL_ID']),list(latomePuMap['SAM']),list(latomePuMap['DET']),list(latomePuMap['AC'])))
latomePuMap.set_index(["DET","AC","SAMtrue"],inplace=True)
print(latomePuMap.head())
latomePuMap.to_pickle("LatomePuMap.pkl")