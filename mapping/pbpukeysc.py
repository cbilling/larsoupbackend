import pandas as pd

from collections import OrderedDict
from pandas.core.indexes.multi import MultiIndex
import json
def multiindex_to_nested_dict(df: pd.DataFrame, value_only = False) -> OrderedDict:
    if isinstance(df.index, MultiIndex):
        #print("hey")
        return OrderedDict((k, multiindex_to_nested_dict(df.loc[k])) for k in df.index.remove_unused_levels().levels[0])
    else:
        if value_only:
            return OrderedDict((k, df.loc[k].values[0]) for k in df.index)
        else:
            d = OrderedDict()
            for idx in df.index:
                d_col = OrderedDict()
                for col in df.columns:
                    d_col[col] = df.loc[idx, col]
                d[idx] = d_col
            return d
        

latomePuMap = pd.read_csv("../forhist.csv")
latomePuMap.set_index(["pbeastKey","SCnum","PU"],inplace=True)
latomePuMap.sort_index(level=["PU","pbeastKey","SCnum"],inplace=True)
for col in latomePuMap.columns:
    if latomePuMap[col].dtype == 'int64':
        latomePuMap[col] = latomePuMap[col].astype(float)

print(latomePuMap.dtypes)
#print(multiindex_to_nested_dict(latomePuMap,value_only=True))
#latomePuMap = latomePuMap["SC_ONL_ID"].to_dict()
latomePuMap = multiindex_to_nested_dict(latomePuMap,value_only=True)


with open('pbpusc.json', 'w') as f:
    f.write(json.dumps(latomePuMap))