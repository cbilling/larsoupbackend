import argparse
import libpbeastpy
import pandas as pd
import time
from datetime import datetime, timedelta
import os
import httpx
import multiprocessing
import pandas as pd
import sys
from numbers import Number
from collections import deque
from collections.abc import Set, Mapping
import json
import pickle


ZERO_DEPTH_BASES = (str, bytes, Number, range, bytearray)


def getsize(obj_0):
    """Recursively iterate to sum size of object & members."""
    _seen_ids = set()
    def inner(obj):
        obj_id = id(obj)
        if obj_id in _seen_ids:
            return 0
        _seen_ids.add(obj_id)
        size = sys.getsizeof(obj)
        if isinstance(obj, ZERO_DEPTH_BASES):
            pass # bypass remaining control flow and return
        elif isinstance(obj, (tuple, list, Set, deque)):
            size += sum(inner(i) for i in obj)
        elif isinstance(obj, Mapping) or hasattr(obj, 'items'):
            size += sum(inner(k) + inner(v) for k, v in getattr(obj, 'items')())
        # Check for custom object instances - may subclass above too
        if hasattr(obj, '__dict__'):
            size += inner(vars(obj))
        if hasattr(obj, '__slots__'): # can have __slots__ with __dict__
            size += sum(inner(getattr(obj, s)) for s in obj.__slots__ if hasattr(obj, s))
        return size
    return inner(obj_0)

#os.environ["PBEAST_SERVER_SSO_SETUP_TYPE"] = "autoupdatekerberos"

parser = argparse.ArgumentParser(description="Start Live Worker with specified partition")

parser.add_argument("--user_id", "-u", type=str, help="user_id")
parser.add_argument("--partition", "-p", type=str, default="ATLAS", help="Name of partition")
parser.add_argument("--runNumber", "-rn", type=str, help="Name of partition")
parser.add_argument("--timestamp", "-t", type=str, default="ATLAS", help="Name of partition")

args = parser.parse_args()
user_id = args.user_id
partition = args.partition
runNumber = args.runNumber
timestamp = args.timestamp

with open('mapping/pbpusc.json', 'r') as read_file:
    latomePuMap = json.loads(read_file.read())

pbeastConnection = libpbeastpy.ServerProxy("https://vm-atlas-tdaq-cc.cern.ch/tbed/pbeast/api")
#pbeastConnection = libpbeastpy.ServerProxy("https://atlasop.cern.ch")
def us_since_epoch(time_str, add_time=0):
    return int(
        datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S").timestamp() * 1e6
    ) + int(add_time * 1e6)

def rates(partition, source = None, since = None, till = None):
    
    t = time.time()
    isclass = "RatesLArC"
    if source == None:
        source = "LArMonParams.LAr.LDPB.LArC_.*"
    sources = ["LArMonParams.LAr.LDPB.LArC_EMBA.*","LArMonParams.LAr.LDPB.LArC_EMBC.*","LArMonParams.LAr.LDPB.LArC_EMECA.*","LArMonParams.LAr.LDPB.LArC_EMECC.*","LArMonParams.LAr.LDPB.LArC_FCAL.*"]

    regex = True
    downsample_interval = 0
    all_publications = False

    if (since == None) and (till == None):
        till = since = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    else:
        till = till.strftime("%Y-%m-%d %H:%M:%S")
        since = since.strftime("%Y-%m-%d %H:%M:%S")
    time_to_grab = us_since_epoch(since, add_time=0)
    time_of_req = datetime.fromtimestamp(time_to_grab / 1e6)
    since_to_grab = us_since_epoch(since, add_time=-30)

    try:
        resultsQueue = multiprocessing.Queue()
        scTime = time.time()
        ps_sc = []
        def sc_worker(source,resQ):
            attribute = "RatesLatomes/RatesLatome/superCell"
            pbSuperCells = pbeastConnection.get_data(
                partition,
                isclass,
                attribute,
                source,
                regex,
                since_to_grab,
                time_to_grab,
                downsample_interval,
                all_publications,
            )
            pbSuperCells = pd.DataFrame(pbSuperCells)["data"][0]
            print(len(list(pbSuperCells.keys())))
            pbSuperCellsDict = {}
            for pbkey in pbSuperCells:
                pbSuperCellsDict[pbkey] = []
                for dataPoint in pbSuperCells[pbkey]:
                    pbSuperCellsDict[pbkey].append([dataPoint.ts,dataPoint.value])
                #break
            resQ.put(pickle.dumps(pbSuperCellsDict))
            print(resQ.empty())
            return
        pbSuperCellsFinal = {}
        for source in sources:
            p = multiprocessing.Process(target = sc_worker,args=(source,resultsQueue))
            p.start()
            ps_sc.append(p)
        print("started")
        for proc in ps_sc:
            proc.join()
        print("done")
        results = []
        for s in sources:
            try:
                results.append(pickle.loads(resultsQueue.get_nowait()))
            except Exception as e:
                print(e)
        print("got results")
        for r in results:
            #print(r.keys())
            pbSuperCellsFinal.update(r)
        #print(pbSuperCellsFinal.keys())
        
        print("sc took: ", time.time()-scTime)
        rateTime = time.time()
        ps_rate = []
        def rate_worker(source,resQ):
            attribute = "RatesLatomes/RatesLatome/streamRates"
            pbRates = pbeastConnection.get_data(
                partition,
                isclass,
                attribute,
                source,
                regex,
                since_to_grab,
                time_to_grab,
                downsample_interval,
                all_publications,
            )
            pbRates = pd.DataFrame(pbRates)["data"][0]
            print(len(list(pbRates.keys())))
            pbRatesDict = {}
            for pbkey in pbRates:
                pbRatesDict[pbkey] = []
                for dataPoint in pbRates[pbkey]:
                    #print([dataPoint.ts,dataPoint.value])
                    pbRatesDict[pbkey].append([dataPoint.ts,dataPoint.value])
                    #break
            
            resQ.put(pickle.dumps(pbRatesDict))
            print(resQ.empty())
            return

        pbRatesFinal = {}
        for source in sources:
            p = multiprocessing.Process(target = rate_worker,args=(source,resultsQueue))
            p.start()
            ps_rate.append(p)
        print("started")
        results = []
        k = 0
        for s in sources:
            try:
                print(k)
                k+=1
                results.append(pickle.loads(resultsQueue.get(timeout=20)))
            except Exception as e:
                print(e)
        print("got results")
        for r in results:
            print(len(list(r.keys())))
            pbRatesFinal.update(r)
        print(len(list(pbRatesFinal.keys())))
        
        for proc in ps_rate:
            proc.join()
        print("done")
        
        
        print("rates took: ", time.time()-rateTime)
               
    except Exception as e:
        
        pbRatesFinal = pbSuperCellsFinal = None
        if isinstance(e, RuntimeError):
            print(e)
            print("reauthentication needed")
            pbRates = "Failed"
        
        else:
            
            print("\n")
            print(e, "no PBEAST data for time", time_of_req)
            print("\n")
            
    print("pbeast",time.time() - t)
    return pbRatesFinal, pbSuperCellsFinal

def histMapping( pbRates, pbSuperCells, timeToCompare, empty=False):

    mappedRates = {}
    t = time.time()
    for pbkey in pbSuperCells:
        pbScOnRightLatome = pbSuperCells[pbkey]
        pbRatesOnRightLatome = pbRates[pbkey]
        #mappingOnRightLatome = latomePuMap[latomePuMap["pbeastKey"] == pbkey]
        #print(pbkey,len(pbRatesOnRightLatome))

        # We want to get the first time index that passes the time we are looking at
        # Then we will go one index back, to get the oldest time, that isn't in
        # in the "future"
        for i in range(len(pbRatesOnRightLatome)):
            
            timeForThisStep = pbRatesOnRightLatome[i][0]
            oldestTime = (timeForThisStep / 1e6) >= timeToCompare
            enoughData = (len(pbScOnRightLatome) >= abs(i)) and (len(pbRatesOnRightLatome) >= abs(i))
            if oldestTime and enoughData:
                timeIndex = i - 1
                pbScNum = pbScOnRightLatome[timeIndex][-1]
                #mappingOnRightSC = mappingOnRightLatome[mappingOnRightLatome["SCnum"] == pbScNum]
                rateList = pbRatesOnRightLatome[timeIndex][-1]
                
                for pu, rate in enumerate(rateList):
                    try:
                        sc_onl_id = latomePuMap[pbkey][pbScNum + 1][pu + 1]["SC_ONL_ID"]

                        mappedRates[int(sc_onl_id)] = rate
                        
                    except:
                        pass
                continue
                    
    return mappedRates

def batchTimes(startDT,endDT):

    #startDT = datetime.strptime(startDT,"%Y-%m-%d %H:%M:%S")
    #endDT = datetime.strptime(endDT,"%Y-%m-%d %H:%M:%S")
    
    # Define the time interval (10 minutes)
    interval = timedelta(minutes=30)

    # Initialize an empty list to store the generated datetimes
    datetime_list = []

    # Start with the start datetime and add the interval until reaching or exceeding the end datetime
    current_datetime = startDT
    while current_datetime <= endDT:
        datetime_list.append(current_datetime)
        current_datetime += interval

    return datetime_list

def batchedMapping(batchStart,batchEnd,lastBatch,source):
    
    # Need to go through this batch and get the state of the system for every second
    currDT = datetime.timestamp(batchStart)
    endDT = datetime.timestamp(batchEnd)
    pbRates, pbSuperCells = rates(partition,source = source, since=batchStart,till=batchEnd)
    if (pbRates != None) and (pbSuperCells != None): 
        batch = []
        while currDT < endDT:

            mappedRates = histMapping(pbRates,pbSuperCells,currDT)
            currDT += 1
            batch.append({"ts":currDT, "data": mappedRates})
        print(source)
        for k in list(mappedRates.keys())[0:10]:
            print(k,mappedRates[k])
        print("fullsize",getsize(batch))
        httpx.post("https://atlas-larmon-dev.cern.ch/dev/LArSoup/updateHistRates",json={"user_id": user_id, "runNumber":runNumber, "data":batch, "done":lastBatch},verify=False)
        

minBefReq = 5
minAftReq = 25
ts = datetime.fromtimestamp(int(timestamp))
since = ts - timedelta(minutes = minBefReq)
till = ts + timedelta(minutes = minAftReq)

total_map = time.time()

batches = batchTimes(since,till)
ps = []
#sources = ["LArMonParams.LAr.LDPB.LArC_EMBA.*","LArMonParams.LAr.LDPB.LArC_EMBC.*","LArMonParams.LAr.LDPB.LArC_EMECA.*","LArMonParams.LAr.LDPB.LArC_EMECC.*","LArMonParams.LAr.LDPB.LArC_FCAL.*"]
sources = ["LArMonParams.LAr.LDPB.LArC_.*"]
for i in range(len(batches) - 1):
    for source in sources:
        
        pastEndDT = False
        if i == (len(batches) - 1):
            pastEndDT = True
        batchedMapping(batches[i],batches[i+1],pastEndDT,source)
        #p = multiprocessing.Process(target=batchedMapping,args=(batches[i],batches[i+1],pastEndDT,source))
        #ps.append(p)
        #p.start()
#for proc in ps:
#    proc.join()
print("totalmapping",time.time() - total_map)
